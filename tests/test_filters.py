#!/usr/bin/env python3

import unittestdimple as unittest

import dimple

class FilterTests(unittest.TestCase):

	class NoPublic:
		def __init__(self):
			pass
		def _private_method_1(self):
			pass
		def _private_method_2(self):
			pass

	class OnlyPublic:
		def public_method_1(self):
			pass
		def public_method_2(self):
			pass

	class SomePublicSomePrivate(NoPublic, OnlyPublic):
		pass


	def test_class_with_no_public_attributes(self):
		a = dimple.public(FilterTests.NoPublic)
		self.assertEqual(len(a), 0)

	def test_instance_with_no_public_attributes(self):
		obj = FilterTests.NoPublic()
		a = dimple.public(obj)
		self.assertEqual(len(a), 0)


	def test_class_with_only_public_attributes(self):
		a = dimple.public(FilterTests.OnlyPublic)
		self.assertEqual(len(a), 2)

	def test_instance_with_only_public_attributes(self):
		obj = FilterTests.OnlyPublic()
		a = dimple.public(obj)
		self.assertEqual(len(a), 2)


	def test_class_with_some_public_attributes(self):
		a = dimple.public(FilterTests.SomePublicSomePrivate)
		self.assertEqual(len(a), 2)

	def test_instance_with_no_public_attributes(self):
		obj = FilterTests.SomePublicSomePrivate()
		a = dimple.public(obj)
		self.assertEqual(len(a), 2)




if __name__ == "__main__":
	unittest.main()
