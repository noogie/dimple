#!/usr/bin/env python3

import unittestdimple as unittest
import os

import dimple


class CatalogTests(unittest.TestCase):

	def test_mammals_with_path(self):
		# Create catalog from the mammals directory.
		# Expect the keys to be the module name, and values to be the modules

		relative = "./modules/animals/mammals"
		absolute = os.path.abspath(relative)
		for path in relative, absolute:
			with self.subTest(path=path):
				modules = dimple.catalog(path=path)
				print(modules)
				self.assertEqual(len(modules), 3)
				for k in ('cat', 'dog', 'cow'):
					self.assertTrue(k in modules)
					self.assertEqual(modules[k].__name__, k)

	def test_mammals_with_path_and_key_str(self):
		# Create catalog from the mammals directory.
		# Expect the keys to be the value of each module's "key" attr, and values to be the modules
		relative = "./modules/animals/mammals"
		absolute = os.path.abspath(relative)
		for path in relative, absolute:
			with self.subTest(path=path):
				modules = dimple.catalog(path=path, key="key")
				print(modules)
				self.assertEqual(len(modules), 3)
				for k in ('cat', 'dog', 'cow'):
					key = f"{k.capitalize()}Module"
					print(key)
					self.assertTrue(key in modules)
					self.assertEqual(modules[key].__name__, k)

	def test_mammals_with_path_and_callable_key(self):
		# Create catalog from the mammals directory.
		# Expect the keys to be the value of each module's "key" attr, and values to be the modules
		relative = "./modules/animals/mammals"
		absolute = os.path.abspath(relative)
		for path in relative, absolute:
			for key in str.upper, lambda basename_without_ext, **_ : basename_without_ext.upper():
				with self.subTest(path=path, key=key):
					modules = dimple.catalog(path=path, key=key)
					print(modules)
					self.assertEqual(len(modules), 3)
					for k in ('cat', 'dog', 'cow'):
						key = k.upper()
						print(key)
						self.assertTrue(key in modules)
						self.assertEqual(modules[key].__name__, k)

	def test_mammals_with_path_and_attr_str(self):
		# Create catalog of Animal modules from the mammals directory
		# Expect the keys to be the module name, and values to be each module's respective "Animal" class
		relative = "./modules/animals/mammals"
		absolute = os.path.abspath(relative)
		for path in relative, absolute:
			with self.subTest(path=path):
				modules = dimple.catalog(path=path, attr="Animal")
				print(modules)
				self.assertEqual(len(modules), 3)
				for k in ('cat', 'dog', 'cow'):
					self.assertTrue(k in modules)
					self.assertEqual(modules[k].__name__, "Animal")

	def test_mammals_with_files(self):
		# Create catalog from a list of files from the mammals directory
		# Expect the keys to be the module name, and values to be the modules
		oldcwd = os.getcwd()
		os.chdir("./modules/animals/mammals")
		files = os.listdir(".")
		modules = dimple.catalog(files=files)
		os.chdir(oldcwd)
		print(modules)
		self.assertEqual(len(modules), 3)
		for k in ('cat', 'dog', 'cow'):
			self.assertTrue(k in modules)
			self.assertEqual(modules[k].__name__, k)

	def test_mammals_with_path_and_files(self):
		# Create catalog from a list of files from the mammals directory
		# Expect the keys to be the module name, and values to be the modules
		relative = "./modules/animals/mammals"
		absolute = os.path.abspath(relative)
		for path in relative, absolute:
			with self.subTest(path=path):
				files = os.listdir(path)
				modules = dimple.catalog(path=path, files=files)
				print(modules)
				self.assertEqual(len(modules), 3)
				for k in ('cat', 'dog', 'cow'):
					self.assertTrue(k in modules)
					self.assertEqual(modules[k].__name__, k)

	def test_birds_with_path(self):
		# Create catalog from the birds directory.
		# Expect the keys to be the module name, and values to be the modules
		relative = "./modules/animals/birds"
		absolute = os.path.abspath(relative)
		for path in relative, absolute:

			with self.subTest(path=path):
				modules = dimple.catalog(path=path)
				print(modules)
				self.assertEqual(len(modules), 2)
				for k in ('chicken', 'pigeon'):
					self.assertTrue(k in modules)
					self.assertEqual(modules[k].__name__, k)

	def test_animals_with_path(self):
		# Create catalog from the animals directory.
		# Expect nothing since the modules are in the subdirectories and recursive was not set
		relative = "./modules/animals"
		absolute = os.path.abspath(relative)
		for path in relative, absolute:
			with self.subTest(path=path):
				modules = dimple.catalog(path=path)
				print(modules)
				self.assertEqual(len(modules), 0)

	def test_animals_with_path_and_recursive_True(self):
		# Create catalog from the animals directory.
		# Expect mammals and birds
		relative = "./modules/animals"
		absolute = os.path.abspath(relative)
		for path in relative, absolute:
			with self.subTest(path=path):
				modules = dimple.catalog(path=path, recursive=True)
				print(modules)
				self.assertEqual(len(modules), 5)
				for k in ('cat', 'dog', 'cow', 'chicken', 'pigeon'):
					self.assertTrue(k in modules)
					self.assertEqual(modules[k].__name__, k)

	def test_animals_with_path_and_recursive_True_and_each(self):
		# Create catalog from the animals directory, with an each function that sets all the available arguments as attributes on each module
		# Expect mammals and birds, and attributes to be set.
		relative = "./modules/animals"
		absolute = os.path.abspath(relative)

		def each(module, **kwargs):
			for k, v in kwargs.items():
				setattr(module, k, v)

		for path in relative, absolute:
			with self.subTest(path=path):
				modules = dimple.catalog(path=path, recursive=True, each=each)
				print(modules)
				self.assertEqual(len(modules), 5)
				for k, v in modules.items():
					self.assertTrue(hasattr(v, 'basename'))
					self.assertTrue(hasattr(v, 'dirname'))
					self.assertTrue(hasattr(v, 'file'))

					self.assertEqual(v.basename, f"{k}.py")
					if k in ("cat", "dog", "cow"):
						self.assertTrue(v.file.endswith(f"/modules/animals/mammals/{k}.py"), f"key:{k} file:{v.file}")
						self.assertTrue(v.dirname.endswith("/modules/animals/mammals"), v.dirname)
					elif k in ("chicken", "pigeon"):
						self.assertTrue(v.file.endswith(f"/modules/animals/birds/{k}.py"), f"key:{k} file:{v.file}")
						self.assertTrue(v.dirname.endswith("/modules/animals/birds"), v.dirname)

if __name__ == "__main__":
	unittest.main()
