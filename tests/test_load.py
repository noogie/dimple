#!/usr/bin/env python3

import unittestdimple as unittest

import dimple


class LoadTests(unittest.TestCase):

	def test_foo_py(self):
		# foo.py with no attributes should give the module
		Foo = dimple.load("./modules/foo.py")
		print(f"module: '{Foo}'")
		self.assertTrue(str(Foo).startswith("<module 'foo' from '"))
		self.assertTrue(str(Foo).endswith("/modules/foo.py'>"))

	def test_foo_py_with_attr_Foo(self):
		# foo.py has only one class defined, and we're explicitly requesting it
		Foo = dimple.load("./modules/foo.py", attr="Foo")
		print(f"class: '{Foo}'")
		self.assertEqual(str(Foo), "<class 'foo.Foo'>")

	def test_foobar_py_with_attr_Foo(self):
		# foobar.py has two classes defined, and we're explicitly requesting Foo
		Foo = dimple.load("./modules/foobar.py", attr="Foo")
		print(f"class: '{Foo}'")
		self.assertEqual(str(Foo), "<class 'foobar.Foo'>")

	def test_foobar_py_with_attr_Bar(self):
		# foobar.py has two classes defined, and we're explicitly requesting Bar
		Bar = dimple.load("./modules/foobar.py", attr="Bar")
		print(f"class: '{Bar}'")
		self.assertEqual(str(Bar), "<class 'foobar.Bar'>")


	def test_foo_py_with_attr_Foo_and_call_True(self):
		# Request an instance of Foo
		foo = dimple.load("./modules/foo.py", attr="Foo", call=True)
		print(f"instance: '{foo}'")
		self.assertTrue(str(foo).startswith("<foo.Foo object at "))

	def test_foobar_py_with_attr_Foo_and_call_True(self):
		# Request an instance of Foo
		foo = dimple.load("./modules/foobar.py", attr="Foo", call=True)
		print(f"instance: '{foo}'")
		self.assertTrue(str(foo).startswith("<foobar.Foo object at "))


	def test_funcs_py_with_attr_func1(self):
		func1 = dimple.load("./modules/funcs.py", attr="func1")
		print(f"func: '{func1}'")
		self.assertTrue(str(func1).startswith("<function func1 at"))

	def test_funcs_py_with_attr_func1_and_call_True(self):
		func1result = dimple.load("./modules/funcs.py", attr="func1", call=True)
		print(f"func: '{func1result}'")
		self.assertTrue(func1result, "FUNC1")

	def test_raise_name_error_py(self):
		with self.assertRaises(NameError):
			_ = dimple.load("./modules/raisers/raise_NameError.py", attr="_")

	def test_raise_attribute_error_py(self):
		with self.assertRaises(AttributeError):
			_ = dimple.load("./modules/raisers/raise_AttributeError.py", attr="_")



if __name__ == "__main__":
	unittest.main()
