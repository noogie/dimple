#!/usr/bin/env python3

import unittestdimple as unittest
import os

import dimple

class CatalogErrorTests(unittest.TestCase):

	def test_raisers_with_error_RAISE(self):
		# Create catalog from the raisers directory

		relative = "./modules/raisers"
		absolute = os.path.abspath(relative)
		for path in [ relative, absolute ]:
			with self.subTest(path=path):
				with self.assertRaises(Exception):
					modules = dimple.catalog(path=path)

	def test_raisers_with_error_tuple_RAISE_and_empty_list(self):
		# Create catalog from the raisers directory. Expect an empty catalog, since all imports will have failed and been silenty ignored

		relative = "./modules/raisers"
		absolute = os.path.abspath(relative)
		for path in [ relative, absolute ]:
			with self.subTest(path=path):
				modules = dimple.catalog(path=path, error=(dimple.RAISE,))
				self.assertEqual(len(modules), 0)

	def test_raisers_with_error_tuple_RAISE_and_ValueError(self):
		# Create catalog from the raisers directory, which has one that will raise a ValueError
		relative = "./modules/raisers"
		absolute = os.path.abspath(relative)
		for path in [ relative, absolute ]:
			with self.subTest(path=path):
				with self.assertRaises(ValueError):
					modules = dimple.catalog(path=path, error=(dimple.RAISE, ValueError))

	def test_raisers_with_error_tuple_RAISE_and_an_exception_that_wont_be_raised(self):
		# Create catalog from the raisers directory. Expect an empty catalog, since all imports will have failed and be silenty ignored

		class UnusedError(Exception):
			pass

		relative = "./modules/raisers"
		absolute = os.path.abspath(relative)
		for path in [ relative, absolute ]:
			with self.subTest(path=path):
				modules = dimple.catalog(path=path, error=(dimple.RAISE, UnusedError))
				self.assertEqual(len(modules), 0)

	def test_raisers_with_callable_error(self):

		def error(ex, *, basename_without_ext, **kwargs):
			print(f"error: {locals()}")
			self.assertEqual(basename_without_ext[6:], type(ex).__name__)

		relative = "./modules/raisers"
		absolute = os.path.abspath(relative)
		for path in [ relative, absolute ]:
			with self.subTest(path=path):
				modules = dimple.catalog(path=path, error=error)
				self.assertEqual(len(modules), 0)



if __name__ == "__main__":
	unittest.main()
