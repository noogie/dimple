#!/usr/bin/env bash

cd $(git rev-parse --git-dir)/../tests

for f in test_*.py
do
	echo "$0: Running test file $f ..."
	if [[ "$f" == *"reload"* ]]
	then
		B="-B"
	else
		B=
	fi
	python3 $B "$f" "$@" 2>&1 | sed 's/^/    /g'
	result=${PIPESTATUS[0]}
	if [ $result -ne 0 ]
	then
		echo "$0: Exiting due to failure ($f returned exit code $result)"
		exit $result
	fi
	echo
done
echo "$0: Success!"
