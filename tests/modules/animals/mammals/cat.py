key = "CatModule"

class Cat:

	key = "CatClass"

	@staticmethod
	def Key():
		return Cat.key

	def speak(self):
		return "Meow"

class Animal(Cat):
	pass
