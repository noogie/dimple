key = "CowModule"

class Cow:

	key = "CowClass"

	@staticmethod
	def Key():
		return Cow.key

	def speak(self):
		return "Moo"

class Animal(Cow):
	pass
