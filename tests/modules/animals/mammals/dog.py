key = "DogModule"

class Dog:

	key = "DogClass"

	@staticmethod
	def Key():
		return Dog.key

	def speak(self):
		return "Woof"

class Animal(Dog):
	pass
