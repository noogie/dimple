key = "PigeonModule"

class Pigeon:

	key = "PigeonClass"

	@staticmethod
	def Key():
		return Pigeon.key

	def speak(self):
		return "Coo"

class Animal(Pigeon):
	pass
