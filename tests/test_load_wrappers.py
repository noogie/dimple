#!/usr/bin/env python3

__pydoc__ = { __file__: False }

import unittestdimple as unittest

import dimple


class NotTested:

	class ClsAndInstanceTests(unittest.TestCase):

		def assertDimpleLoadReturnValueOk(self, o, s):
			self.assertIsNotNone(o)
			if self.func is dimple.instance:
				self.assertTrue(str(o).startswith(f"<{s} object at "))
			else:
				self.assertTrue(str(o).startswith(f"<class '{s}'>"))

		def test_foo_py(self):
			# Request the first class found in foo.py
			foo = self.func("./modules/foo.py")
			print(f"{self.prefix}: {foo}")
			self.assertDimpleLoadReturnValueOk(foo, "foo.Foo")

		def test_foo_py(self):
			# Request the first class found in foobar.py
			foo = self.func("./modules/foobar.py")
			print(f"{self.prefix}: {foo}")
			self.assertDimpleLoadReturnValueOk(foo, "foobar.Foo")

		def test_foo_py_with_attr_foo(self):
			# Explicity request Foo from foo.py
			foo = self.func("./modules/foo.py", attr="Foo")
			print(f"{self.prefix}: {foo}")
			self.assertDimpleLoadReturnValueOk(foo, "foo.Foo")

		def test_foobar_py_with_attr_foo(self):
			# Explicity request Foo from foobar.py
			foo = self.func("./modules/foobar.py", attr="Foo")
			print(f"{self.prefix}: {foo}")
			self.assertDimpleLoadReturnValueOk(foo, "foobar.Foo")

		def test_dict_subclass_py(self):
			# Request the first class in sub.py (Just to prove a DictSubclass instance isn't returned)
			sub = self.func("./modules/dict_subclass.py")
			print(f"{self.prefix}: {sub}")
			self.assertDimpleLoadReturnValueOk(sub, "dict_subclass.NotADictSubclass")

		def test_dict_subclass_py_with_super_dict(self):
			# Request first subclass of dict in sub.py
			sub = self.func("./modules/dict_subclass.py", super=dict)
			print(f"{self.prefix}: {sub}")
			self.assertDimpleLoadReturnValueOk(sub, "dict_subclass.DictSubclass")

		def test_from_py(self):
			# Request the first class found in from.py, which contains an 'from x import y'
			_from = self.func("./modules/from.py")
			print(f"{self.prefix}: {_from}")
			self.assertDimpleLoadReturnValueOk(_from, "from.From")

		def test_funcs_py(self):
			# Request the first class found in funcs.py, which contains only functions
			none = self.func("./modules/funcs.py")
			print(f"{self.prefix}: {none}")
			self.assertIsNone(none)


class ClsTests(NotTested.ClsAndInstanceTests):
	def setUp(self):
		self.prefix = "cls"
		self.func = dimple.cls
		super().setUp()

class InstanceTests(NotTested.ClsAndInstanceTests):
	def setUp(self):
		self.prefix = "instance"
		self.func = dimple.instance
		super().setUp()

if __name__ == "__main__":
	unittest.main()
