#!/usr/bin/env python3

import logging
import unittestdimple as unittest

import dimple


class NotTested:

	class ClassesAndInstancesTests(unittest.TestCase):

		def assertDimpleCatalogOk(self, catalog, *, length, keys=None, classes=None):
			print(catalog)
			self.assertIsNotNone(catalog)
			self.assertEqual(len(catalog), length)

			if keys is not None:
				self.assertEqual(len(catalog), len(keys))
				for k, v in catalog.items():
					self.assertIn(k, keys)
					keys.remove(k)
				self.assertEqual(len(keys), 0)

			if classes is not None:
				for k, v in catalog.items():
					print(f"k:'{k}' v:'{v}'")
					self.assertIn(cls, catalog)

#					if self.func is dimple.instances:
#						self.assertTrue(str(v).startswith(f"<{s} object at "))
#					else:
#						self.assertTrue(str(v).startswith(f"<class '{s}'>"))

		def test_mammals(self):
			self.logger.debug("\n----------------------------------------------------------------")
			self.logger.debug(f"{self} begin")
			catalog = self.func(path="./modules/animals/mammals")
			self.assertDimpleCatalogOk(catalog, length=3)
			self.logger.debug(f"{self} end")

		def test_mammals_with_attr_Animals(self):
			self.logger.debug("\n----------------------------------------------------------------")
			self.logger.debug(f"{self}.test_animals_with_attr_Animals: begin")
			catalog = self.func(path="./modules/animals/mammals", attr='Animal')
			self.assertDimpleCatalogOk(catalog, length=3, keys=['cat', 'dog', 'cow'])
			self.logger.debug(f"{self} end")

		def test_animals(self):
			self.logger.debug("\n----------------------------------------------------------------")
			self.logger.debug(f"{self} begin")
			catalog = self.func(path="./modules/animals", recursive=True)
			self.assertDimpleCatalogOk(catalog, length=5)
			self.logger.debug(f"{self} end")

		def test_animals_with_attr_Animals(self):
			self.logger.debug("\n----------------------------------------------------------------")
			self.logger.debug(f"{self} begin")
			catalog = self.func(path="./modules/animals", recursive=True, attr='Animal')
			self.assertDimpleCatalogOk(catalog, length=5, keys=['cat', 'dog', 'cow', 'chicken', 'pigeon'])
			self.logger.debug(f"{self} end")

class ClassesTests(NotTested.ClassesAndInstancesTests):
	def setUp(self):
		self.logger = logging.getLogger('dimple')
		self.prefix = "classes"
		self.func = dimple.classes
		super().setUp()

class InstancesTests(NotTested.ClassesAndInstancesTests):
	def setUp(self):
		self.logger = logging.getLogger('dimple')
		self.prefix = "instances"
		self.func = dimple.instances
		super().setUp()

class IndirectClassesTests(NotTested.ClassesAndInstancesTests):
	def setUp(self):
		self.logger = logging.getLogger('dimple')
		self.prefix = "indirect_classes"
		self.func = lambda *, path=None, recursive=False, **kwargs: dimple.modules(path=path, recursive=recursive).classes(**kwargs)
		super().setUp()

class IndirectInstancesTests(NotTested.ClassesAndInstancesTests):
	def setUp(self):
		self.logger = logging.getLogger('dimple')
		self.prefix = "indirect_instances"
		self.func = lambda *, path=None, recursive=False, **kwargs: dimple.modules(path=path, recursive=recursive).instances(**kwargs)
		super().setUp()


if __name__ == "__main__":
	unittest.main()
