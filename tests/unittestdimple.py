import logging
import unittest
import sys

sys.path.insert(0, "..")

class TestCase(unittest.TestCase):
	pass

def main():
	if '--debug' in sys.argv:
		logging.basicConfig()
		logger = logging.getLogger('dimple')
		logger.setLevel(logging.DEBUG)
		sys.argv.remove('--debug')

	unittest.main(buffer=True, failfast=True)
