#!/usr/bin/env -S python3 -B

import hashlib
import logging
import os
import os.path
import shutil
from   time import sleep
import unittestdimple as unittest

import dimple


class CatalogReloadTests(unittest.TestCase):

	def hash(self, buffer):
		hasher = hashlib.md5()
		hasher.update(buffer)
		return hasher.hexdigest()

	def copy(self, src, dst, *, value=None):
		shutil.copy(src, dst)
		with open(src, 'rb') as f:
			hash1 = self.hash(f.read())
		with open(dst, 'rb') as f:
			hash2 = self.hash(f.read())
		self.assertEqual(hash1, hash2)
		if value is not None:
			with open(dst) as f:
				self.assertTrue(str(value) in f.read())

	def test_(self):
		# Create catalog from the raisers directory

		logger = logging.getLogger("dimple")

		src_dir   = "./modules/reload"
		relative  = os.path.join(src_dir, "tmp")
		absolute  = os.path.abspath(relative)

		for dst_dir in [ relative, absolute ]:
			with self.subTest(dst_dir=dst_dir):
				try:
					try:
						os.mkdir(dst_dir)
					except FileExistsError:
						pass
					self.assertTrue(os.path.isdir(dst_dir))

					dst_file = os.path.join(dst_dir, "reload.py")

					for x in range(2):

						sleep(0.5)

						i = x + 1

						src_file = os.path.join(src_dir, f"reload{i}.py")
						logger.debug(f"TEST: =======================================")
						logger.debug(f"TEST: [{x}] Copying {src_file} to {dst_file} ...")
						logger.debug(f"TEST: =======================================")

						self.copy(src_file, dst_file)

						if not x:
							catalog = dimple.classes(path=dst_dir, reload=True)
							self.assertEqual(len(catalog), 1)

						instance = catalog["reload"]()
						logger.debug(f"TEST: =======================================")
						logger.debug(f"TEST: [{x}] instance {instance}")
						logger.debug(f"TEST: =======================================")
						self.assertEqual(instance.version(), i, f"src:{src_file} instance.version():{instance.version()} expected:{i}")

						logger.debug(f"TEST: =======================================")
						logger.debug(f"TEST: OK")
						logger.debug(f"TEST: =======================================")

				except Exception as ex:
					shutil.rmtree(dst_dir, ignore_errors=True)
					raise ex

if __name__ == "__main__":
	unittest.main()
